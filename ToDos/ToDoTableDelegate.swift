//
//  File.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/24/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import Foundation

protocol ToDosDelegate: class {
        
    func setToDos(toDosList: [ToDoItem])
    
    func reloadToDosTable()
    
    func alertNoInernetConnection()
}
