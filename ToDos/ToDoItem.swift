//
//  ToDoItem.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/24/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import Foundation

class ToDoItem {
    var isCompleted: Int?
    var id: Int?
    var title: String?
    var userId: Int?
}
