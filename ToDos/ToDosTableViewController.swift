//
//  ToDosTableViewController.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/24/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import UIKit

class ToDosTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var toDos = [ToDoItem]()
    var toDosServiceProvider = ToDosServiceProvider()
    
    @IBOutlet weak var toDosTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toDosTable.delegate = self
        toDosTable.dataSource = self
        toDosServiceProvider.toDosTable = self
        toDosServiceProvider.getAllToDos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDos.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "toDoCell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = toDos[indexPath.row].title
        (cell.viewWithTag(1) as! UILabel).numberOfLines = 0
        (cell.viewWithTag(1) as! UILabel).sizeToFit()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var toDoDetails =  self.storyboard?.instantiateViewController(withIdentifier: "detailsView") as! ToDoDetailsViewController
        toDoDetails.toDoItemToShow = toDos[indexPath.row]
        self.navigationController?.pushViewController(toDoDetails, animated: true)
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}

extension ToDosTableViewController: ToDosDelegate {
    
    func alertNoInernetConnection() {
        let alert = UIAlertController(title: "No Internet Connection", message: "Please check your internet connection and then try again", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            action in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.shared.open(settingsUrl)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setToDos(toDosList: [ToDoItem]) {
        toDos = toDosList
    }
    
    func reloadToDosTable() {
        toDosTable.reloadData()
    }
}

