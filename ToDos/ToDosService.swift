//
//  ViewController.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/24/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import UIKit
import Moya

enum ToDosService {
    
    case getToDoList
    
}

extension ToDosService: TargetType {

    var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }
    
    var path: String {
        switch self {
        case .getToDoList:
            return "/todos"
        }
    }
    
    //Request Method
    var method: Moya.Method {
        switch self {
        case .getToDoList:
            return .get
        }
    }
    
    //Request Parameters
    var task: Task {
        switch self {
        
        //No Parameters Needed
        case .getToDoList:
            return .requestPlain
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getToDoList:
            return Data("Unit Test".utf8)
    
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

