//
//  ToDosServiceProvider.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/26/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import Foundation
import Moya

class ToDosServiceProvider {
    var toDosList = [ToDoItem]()
    var toDosTable: ToDosDelegate?
    let provider = MoyaProvider<ToDosService>()
    var jsonResult = Array<Dictionary<String, AnyObject>>()
    
    func getAllToDos() {
        
        provider.request(.getToDoList) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                    let json = try moyaResponse.mapJSON()
                    self.jsonResult = json as! Array<Dictionary<String, AnyObject>>
                    
                    for index in 0...self.jsonResult.count-1 {
                        let todoItem = ToDoItem()
                        todoItem.id = self.jsonResult[index]["id"] as? Int
                        todoItem.isCompleted = self.jsonResult[index]["completed"] as? Int
                        todoItem.title = self.jsonResult[index]["title"] as? String
                        todoItem.userId = self.jsonResult[index]["userId"] as? Int
                        
                        self.toDosList.append(todoItem)
                        //print(result![index]["title"])
                    }
                
                    self.toDosTable?.setToDos(toDosList: self.toDosList)
                    self.toDosTable?.reloadToDosTable()
                    
                } catch {}
                
            case let .failure(error):
                self.toDosTable?.alertNoInernetConnection()
            }
        }
    }
}
