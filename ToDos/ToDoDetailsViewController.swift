//
//  ToDoViewController.swift
//  ToDos
//
//  Created by Fadwa Zaghloul on 4/24/18.
//  Copyright © 2018 ITI. All rights reserved.
//

import UIKit

class ToDoDetailsViewController: UIViewController {

    var toDoItemToShow = ToDoItem()
    
    @IBOutlet weak var toDoCompletionLbl: UILabel!
    @IBOutlet weak var userIdLbl: UILabel!
    @IBOutlet weak var toDoIdLbl: UILabel!
    @IBOutlet weak var toDoTitleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toDoCompletionLbl.text = String(describing: toDoItemToShow.isCompleted!)
        userIdLbl.text = String(describing: toDoItemToShow.userId!)
        toDoTitleLbl.text = toDoItemToShow.title
        toDoIdLbl.text = String(describing: toDoItemToShow.id!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
